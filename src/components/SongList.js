import React ,{Component} from 'react';
/*1*/ import { connect } from 'react-redux';

        class SongList extends Component{
        
            renderList(){
                return this.props.songs.map((song) => {
                    return (
                        <div className="item" key={song.title}>
                            <div className="right floated content">
                                <button className="ui button primary">
                                    Select
                                </button>
                                <div className="content">{song.title}</div>
                            </div>
                        </div>
                    )
                })
            };
                
            render(){
                return <div className="ui divided list">{this.renderList()}</div>
            }
            
        }

/*3*/   const mapStateToProps = state => {
        return { state: state.props};
    }

/*2*/   export default connect(mapStateToProps)(SongList);
import {combineReducers} from 'redux';

//Reducer che si occupa di restituire una lista di informazioni riguardo la canzone 
const songReducer = () => {
    return [
        {titolo: 'One', durata: '1308'},
        {titolo: 'Fade to black', durata: '1408'},
        {titolo: 'Nothing else matters', durata: '2308'},
        {titolo: 'Seek n Destroy', durata: '3208'},
        {titolo: 'For Whom The Bell Tolls', durata: '0208'}
    ]
}


const selectedSongReducer = (selectSong=null, action) => {
    if(action.type === 'SONG_SELECTED') {
        return action.payload;
    }
    return selectSong;
};

export const rootReducer = combineReducers({
    songs: songReducer,
    selectedSongs: selectedSongReducer
})

 